# **IIOT INDUSTRY PERSPECTIVE**

## **1.  INDUSTRIAL REVOLUTION**
  
| Industry **1.0** | Industry **2.0** | Industry **3.0** | Industry **4.0** |
| ------ | ------ | ------ | ------ |
| 1784 | 1870 | 1969 | Today |
| Mechanization | Mass Production | Automation | Cyber physical systems | 
| Steam power | Assembly line | Computers and Electronics | Internate of things |
| weaving loom | Electrical energy | - | Network |

## **2.  INDUSTRY 3.0**

         
###  **Hierarchy**  
*  **Enterprise**-Collection of Work centers. 
*  **Work centers**--Collection of stations. 
*  **Station**--Collection of field device and control device. 
*  **Control device**--Control field device. 
*  **Field device**--Machines for products. 

![](https://www.researchgate.net/profile/Alexander_Zureck/publication/327130483/figure/fig2/AS:769763521347588@1560537333709/Interaction-between-hierarchies-Levels-in-Industry-30-and-Industry-40-source.png)

###   **Architecture**
***Sensors -> PLCs -> SCADA and EPR***
*  **Sensors** -- Consists of sensors, actuators, RTU () which are located at various points in the factory and send the data to control devices.
*  **Controllers** -- Includes PLC, CNC, DCS, which collect the data and forward to SCADA and EPR.
*  **SCADA and EPR** --systems which stores the data. Data is stored in Excels and CSV's and rarely get plotted as real-time graphs or charts.
*  **Controllers and Field**-- devices are connected through Fieldbus
 
SCADA AND ERP are guides/softwares which maintain databases and excels.Sensors,controllers,scada combine to form industry 3.0.

###   **Communication protocols**

*  These protocols are usually one on one data transferring
*  These protocols are used by sensors to Send data to PLC's. These protocols are called fieldbus.
*  Protocols
    *   Modbus
    *   Profi NET
    *   CAN Open
    *   Ether CAT

## **3.  INDUSTRY 4.0**
###   **Overview**
*  **Features**
    *  Dashboard
    *   Remote control configuration of devices
    *   Real time event stream processing
    *   Automated Device provisioning, Auto discovery
    *   Remote Web SCADA
    *   Predictive Maintenance
    *   Analytics Workbench with predictive models
    *   Real time alerts & alarms


###   **Architecture**
*   **Factory**
    *   Data from sensors to controllers through Fieldbus and then passing to  SCADA and EPR.
    *   From SCADA through ethernet and from controller through CONTROL BUS, data goes to OPC Server

*   **Edge**
    *   From the OPC server through OPC UA/DA data goes to EDGE i.e IoT gateway,OPC proxy etc.

*   **Cloud**
    *   From EDGE through MQTT, COAP, WEBSOCKET the data is transferred to cloud.
    *   Also from EPR through Resy API data directly transferred to the cloud.

![](https://image.slidesharecdn.com/consumervsindustrialiot-160929091851/95/consumer-vs-industrial-iot-7-638.jpg?cb=1475141095)

###   **Communication protocols**
*   These protocols are especially designed for IOT i.e these are with low bandwidth and store data and send only when the internet will on.
*   Protocols
    *   MQTT
    *   OPC UA
    *   COAP
    *   WEBSOCKET
    *   HTTP
    *   Rest API
    *   AMQP

###   **Problems**
*   **Cost** --Industry 3.0 devices are very Expensive as a factory owner I don't want to switch to Industry 4.0 devices because they are Expensive.
*   **Downtime** --Changing Hardware means a big factory downtime, I don't want to shut down my factory to upgrade devices.
*   **Reliability** --Don't want to invest in devices which are unproven and unreliable.

###   **Solutions**
*   Get data from Industry 3.0 devices/meters/sensors without changes to the original device. And then send the data to the Cloud using Industry 4.0 devices
*   Convert industry 3.0 protocols to industry 4.0 protocols

## **4.  Get data from industry 3.0**
###   **Convert industry 3.0 to industry 4.0 protocols**
*   Challenges in conversion
    *   Expensive Hardware
    *   Lack of Documentation
    *   Preparatory PLC protocols

###   **How to convert**
*   We have a library that helps get data from Industry 3.0 devices and send to Industry 4.0 cloud

## **5. Roadmap**
*   Roadmap for making your own industrial iot product
    *   Identify most popular Industry 3.0 devices.
    *   Study Protocols that these devices Communicate
    *   Get data from the Industry 3.0 devices
    *   Send the data to cloud for Industry 4.0
    
## **6. After sending data to internet**
*   Anlyse the data using various online tools 
    *  **IOT TSDB (Time Series Databses)**-- Store your data in TSDB
            1. Prometheus
            2. Influxdb
    *  **IOT Dashboards**-- View data in dashboards
            1. Graphana
            2. Thingsboard
    *  **IOT Platforms**-- Anlyse your data 
            1. AWS IOT
            2. Google IOT
            3. Azure IOT
            4. Thingsboards
    *  **Get alerts**-- Get alerts based on your data using these platforms
            1. Zaiper
            2. Twilio

## **7. Inside the IOT Cloud**
| Devices | Streams | Listner | Alarms |
| ------ | ------ | ------- | ----- |
| Core of IOT Projects | Information in IOT projects | The Actions man in IOT project | The notifications in IOT projects |
| They are _things_ in IOT. When you send the streams, they are always related to devices | When you send the streams, they are always related to devices | Listner use contex data in expressions and rules. They are executed when related event occurs for the entity _listened_. | They are created by user. they signal when something goes wrong or devics changes its status |
| Manage Devices | Manage Streams | Manage Listeners | Manage Alarms|



